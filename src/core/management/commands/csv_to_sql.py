from django.core.management.base import BaseCommand
from core.models import City, Country, Region
import pandas as pd


def save_city(data):
    city = City()
    city.id = data.city_id
    city.title = city.title_en = data.title_en
    city.title_ru, city.title_es = data.title_ru, data.title_es
    city.country = Country.objects.get(pk=data.country_id)
    city.region = Region.objects.get(pk=int(data.region_id))
    city.save()


def save_country(data):
    country = Country()
    country.id = data.id
    country.title = country.title_en = data.title_en
    country.title_ru, country.title_es = data.title_ru, data.title_es
    country.save()


def save_region(data):
    region = Region()
    region.id = data.id
    region.title = region.title_en = data.title_en
    region.title_ru, region.title_es = data.title_ru, data.title_es
    region.country = Country.objects.get(pk=data.country)
    region.save()


def save_regions():
    data_frame = pd.read_csv(
        "../data/_regions.csv",
        encoding="utf8",
        usecols=["id", "country", "title_ru", "title_en", "title_es"],
    )
    data_frame.dropna(inplace=True)
    for index, row in data_frame.iterrows():
        save_region(row)


def save_cities():
    data_frame = pd.read_csv(
        "../data/_cities.csv",
        encoding="utf8",
        usecols=[
            "city_id",
            "country_id",
            "region_id",
            "title_ru",
            "title_en",
            "title_es",
        ],
    )
    data_frame.dropna(inplace=True)
    print(data_frame.head())
    for index, row in data_frame.iterrows():
        save_city(row)


def save_countries():
    data_frame = pd.read_csv(
        "../data/_countries.csv",
        encoding="utf8",
        usecols=["id", "title_ru", "title_en", "title_es"],
    )
    data_frame.dropna(inplace=True)
    for index, row in data_frame.iterrows():
        save_country(row)


class Command(BaseCommand):
    def handle(self, *args, **options):
        save_countries()
        save_regions()
        save_cities()
