from django.db import models


class Country(models.Model):
    title = models.CharField(max_length=255)

    class Meta:
        ordering = ["pk"]
        verbose_name_plural = "countries"

    def __str__(self):
        return self.title


class Region(models.Model):
    title = models.CharField(max_length=255)
    country = models.ForeignKey("Country", on_delete=models.CASCADE)

    class Meta:
        ordering = ["pk"]

    def __str__(self):
        return f"{self.title} ({self.country})"


class City(models.Model):
    title = models.CharField(max_length=255)
    area = models.CharField(max_length=255, blank=True, null=True)
    region = models.ForeignKey("Region", on_delete=models.CASCADE, blank=True, null=True)
    country = models.ForeignKey("Country", on_delete=models.CASCADE)

    class Meta:
        ordering = ["pk"]
        verbose_name_plural = "cities"

    def __str__(self):
        return (
            f"{self.title} ({self.area})"
            if self.area
            else f"{self.title} ({self.country})"
        )
